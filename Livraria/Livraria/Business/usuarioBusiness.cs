﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria.Business
{
    class usuarioBusiness
    {
        Database.usuarioDatabase usuarioDatabase = new Database.usuarioDatabase();

        public bool Login (string usu, string senha, string email)
        {
            if (string.IsNullOrEmpty(usu))
                throw new ArgumentException("Informe o usuário!");

            if (string.IsNullOrEmpty(senha))
                throw new ArgumentException("Informe a senha!");

            if (string.IsNullOrEmpty(email))
                throw new ArgumentException("Informe o e-mail!");

            bool contem = usuarioDatabase.Login(usu, senha, email);
            return contem;
        }

    }
}
