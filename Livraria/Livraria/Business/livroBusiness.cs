﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria.Business
{
    class livroBusiness
    {
        Database.livroDatabase db = new Database.livroDatabase();

        public void InserirUsuario(Database.Entity.tb_usuario usuario)
        {
            if (usuario.nm_usuario == string.Empty)
                throw new ArgumentException("o nome do usuário não pode está vazio.");
            if (usuario.sn_usuario == string.Empty)
                throw new ArgumentException("a senha do usuário não pode está vazia.");
            if (usuario.em_usuario == string.Empty)
                throw new ArgumentException("o email do usuário não pode está vazio.");
            if (usuario.sb_usuario == string.Empty)
                throw new ArgumentException("o sobrenome do usuário não pode está vazio.");
            if (usuario.us_usuario == string.Empty)
                throw new ArgumentException("o nick do usuário não pode está vazio.");

            db.InserirUsuário(usuario);

        }
        public void InserirLivro(Database.Entity.tb_livro livro)
        {
            if (livro.nm_livro == string.Empty)
                throw new ArgumentException("o nome do livro não pode está vazio.");
            if (livro.nm_editora == string.Empty)
                throw new ArgumentException("a editora do livro não pode está vazia.");
            

            db.InserirLivro(livro);
        }
        public List<Database.Entity.tb_livro> ConsultarLivro(string NomeLivro)
        {
            List<Database.Entity.tb_livro> livro = db.ConsultarLivro(NomeLivro);
            return livro;
        }

        public void AlterarLivro(string nome, string genero, string editora, int id)
        {
            if (nome == string.Empty)
                throw new ArgumentException("o nome do livro não pode está vazio.");
            if (editora == string.Empty)
                throw new ArgumentException("a editora do livro não pode está vazia.");
            if (id == null)
                throw new ArgumentException("o id do livro não pode está vazia.");

            db.AlterarLivro(nome, genero, editora, id);
        }

        public void Remover(int id)
        {
            if (id == null || id == 0)
                throw new ArgumentException("Informe o id");
            Database.livroDatabase a = new Database.livroDatabase();
            a.Remover(id);
        }
    }
}
