﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Inserir
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_usuario usuario = new Database.Entity.tb_usuario();
                usuario.nm_usuario = txtNome.Text;
                usuario.sb_usuario = txtSobrenome.Text;
                usuario.sn_usuario = txtSenha.Text;
                usuario.em_usuario = txtEmail.Text;
                usuario.us_usuario = txtNick.Text;
                usuario.dt_nascimento = dtpDataNascimento.Value;

                Business.livroBusiness livro = new Business.livroBusiness();
                livro.InserirUsuario(usuario);

                MessageBox.Show("Usuário cadastrado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente novamente mais tarde");

            }

        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
