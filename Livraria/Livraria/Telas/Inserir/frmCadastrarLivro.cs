﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Inserir
{
    public partial class frmCadastrarLivro : Form
    {
        public frmCadastrarLivro()
        {
            InitializeComponent();
        }

        private void btnCadastrarLivro_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_livro livro = new Database.Entity.tb_livro();
                livro.nm_livro = txtNomeLivro.Text;
                livro.nm_editora = txtEditora.Text;
                livro.gn_livro = cboGenero.Text;
                livro.lc_livro = dtpLançamento.Value;

                Business.livroBusiness db = new Business.livroBusiness();
                db.InserirLivro(livro);

                MessageBox.Show("Livro inserido com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Tente novamente mais tarde");

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrarLivro_Click_1(object sender, EventArgs e)
        {
                Database.Entity.tb_livro livro = new Database.Entity.tb_livro();
                livro.nm_livro = txtNomeLivro.Text;
                livro.nm_editora = txtEditora.Text;
                livro.gn_livro = cboGenero.Text;
                livro.lc_livro = dtpLançamento.Value;

                Business.livroBusiness db = new Business.livroBusiness();
                db.InserirLivro(livro);

                MessageBox.Show("Livro inserido com sucesso");
        }
    }
}

