﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Outros
{
    public partial class frmCarregamento : Form
    {
        public frmCarregamento()
        {
            InitializeComponent();
        }

        private void TimerCarregamento_Tick(object sender, EventArgs e)
        {
            Hide();
            timerCarregamento.Enabled = false;
            Consultar.frmLogin start = new Consultar.frmLogin();
            start.Show();
        }
    }
}
