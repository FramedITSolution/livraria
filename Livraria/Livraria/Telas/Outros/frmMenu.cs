﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void imgConsultarLivro_Click(object sender, EventArgs e)
        {
            Telas.Consultar.frmConsultarLivro s = new Consultar.frmConsultarLivro();
            s.Show();
        }

        private void imgInserirLivro_Click(object sender, EventArgs e)
        {
            Inserir.frmCadastrarLivro s = new Inserir.frmCadastrarLivro();
            s.Show();
        }

        private void imgAlterarLivro_Click(object sender, EventArgs e)
        {
            Alterar.frmAlterarLivro s = new Alterar.frmAlterarLivro();
            s.Show();
        }

        private void imgRemoverLivro_Click(object sender, EventArgs e)
        {
            Remover.frmRemoverLivro s = new Remover.frmRemoverLivro();
            s.Show();
        }

        private void imgOrganizar_Click(object sender, EventArgs e)
        {
            Outros.frmOrganizarLivro s = new Outros.frmOrganizarLivro();
            s.Show();
        }

        private void imgVerLivros_Click(object sender, EventArgs e)
        {
            Outros.frmVerLivros s = new Outros.frmVerLivros();
            s.Show();
        }
    }
}
