﻿namespace Livraria.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgConsultarLivro = new System.Windows.Forms.PictureBox();
            this.imgInserirLivro = new System.Windows.Forms.PictureBox();
            this.imgAlterarLivro = new System.Windows.Forms.PictureBox();
            this.imgVerLivros = new System.Windows.Forms.PictureBox();
            this.imgOrganizar = new System.Windows.Forms.PictureBox();
            this.imgRemoverLivro = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFechar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarLivro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInserirLivro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterarLivro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVerLivros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOrganizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRemoverLivro)).BeginInit();
            this.SuspendLayout();
            // 
            // imgConsultarLivro
            // 
            this.imgConsultarLivro.Location = new System.Drawing.Point(97, 171);
            this.imgConsultarLivro.Name = "imgConsultarLivro";
            this.imgConsultarLivro.Size = new System.Drawing.Size(100, 50);
            this.imgConsultarLivro.TabIndex = 0;
            this.imgConsultarLivro.TabStop = false;
            this.imgConsultarLivro.Click += new System.EventHandler(this.imgConsultarLivro_Click);
            // 
            // imgInserirLivro
            // 
            this.imgInserirLivro.Location = new System.Drawing.Point(241, 171);
            this.imgInserirLivro.Name = "imgInserirLivro";
            this.imgInserirLivro.Size = new System.Drawing.Size(100, 50);
            this.imgInserirLivro.TabIndex = 1;
            this.imgInserirLivro.TabStop = false;
            this.imgInserirLivro.Click += new System.EventHandler(this.imgInserirLivro_Click);
            // 
            // imgAlterarLivro
            // 
            this.imgAlterarLivro.Location = new System.Drawing.Point(376, 171);
            this.imgAlterarLivro.Name = "imgAlterarLivro";
            this.imgAlterarLivro.Size = new System.Drawing.Size(100, 50);
            this.imgAlterarLivro.TabIndex = 2;
            this.imgAlterarLivro.TabStop = false;
            this.imgAlterarLivro.Click += new System.EventHandler(this.imgAlterarLivro_Click);
            // 
            // imgVerLivros
            // 
            this.imgVerLivros.Location = new System.Drawing.Point(376, 269);
            this.imgVerLivros.Name = "imgVerLivros";
            this.imgVerLivros.Size = new System.Drawing.Size(100, 50);
            this.imgVerLivros.TabIndex = 5;
            this.imgVerLivros.TabStop = false;
            this.imgVerLivros.Click += new System.EventHandler(this.imgVerLivros_Click);
            // 
            // imgOrganizar
            // 
            this.imgOrganizar.Location = new System.Drawing.Point(241, 269);
            this.imgOrganizar.Name = "imgOrganizar";
            this.imgOrganizar.Size = new System.Drawing.Size(100, 50);
            this.imgOrganizar.TabIndex = 4;
            this.imgOrganizar.TabStop = false;
            this.imgOrganizar.Click += new System.EventHandler(this.imgOrganizar_Click);
            // 
            // imgRemoverLivro
            // 
            this.imgRemoverLivro.Location = new System.Drawing.Point(97, 269);
            this.imgRemoverLivro.Name = "imgRemoverLivro";
            this.imgRemoverLivro.Size = new System.Drawing.Size(100, 50);
            this.imgRemoverLivro.TabIndex = 3;
            this.imgRemoverLivro.TabStop = false;
            this.imgRemoverLivro.Click += new System.EventHandler(this.imgRemoverLivro_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Kristen ITC", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkViolet;
            this.label1.Location = new System.Drawing.Point(222, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 44);
            this.label1.TabIndex = 7;
            this.label1.Text = "MENU";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.BackColor = System.Drawing.Color.Transparent;
            this.lblFechar.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Red;
            this.lblFechar.Location = new System.Drawing.Point(545, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(29, 29);
            this.lblFechar.TabIndex = 8;
            this.lblFechar.Text = "X";
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Livraria.Properties.Resources.background_Menu_Livraria;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(586, 480);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgVerLivros);
            this.Controls.Add(this.imgOrganizar);
            this.Controls.Add(this.imgRemoverLivro);
            this.Controls.Add(this.imgAlterarLivro);
            this.Controls.Add(this.imgInserirLivro);
            this.Controls.Add(this.imgConsultarLivro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmMenu";
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarLivro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInserirLivro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterarLivro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVerLivros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOrganizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRemoverLivro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgConsultarLivro;
        private System.Windows.Forms.PictureBox imgInserirLivro;
        private System.Windows.Forms.PictureBox imgAlterarLivro;
        private System.Windows.Forms.PictureBox imgVerLivros;
        private System.Windows.Forms.PictureBox imgOrganizar;
        private System.Windows.Forms.PictureBox imgRemoverLivro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFechar;
    }
}