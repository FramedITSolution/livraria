﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Outros
{
    public partial class frmEsqueceuSenha : Form
    {
        public frmEsqueceuSenha()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            Enviar();

        }

        public string Enviar()
        {
            string codigo = GerarSenha();
            Task.Factory.StartNew(() =>
            {
                System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage();

                string remetente = "frameditsolution123@gmail.com";
                string senha = "framedfis123";

                // Configura Remetente, Destinatário
                email.From = new System.Net.Mail.MailAddress(remetente);
                email.To.Add(txtEmail.Text);
                
                // Configura Assunto, Corpo e se o Corpo está em Html
                email.Subject = "Código de Alteração";
                email.Body = "O código é" + codigo + " " + txtUsuario.Text;
                email.IsBodyHtml = true;

                // Configura os parâmetros do objeto SMTP
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;

                // Atribui credenciais
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(remetente, senha);


                // Envia a mensagem
                smtp.Send(email);
                
            });
            return codigo;
        }
            private string GerarSenha()
        {
            int tamanhoSenha = 10;
            string senha = string.Empty;
            for (int i = 0; i < tamanhoSenha; i++)
            {
                Random r = new Random();
                int codigo = Convert.ToInt32(r.Next(48, 122).ToString());
                if ((codigo >= 48 && codigo <= 57) || (codigo >= 97 && codigo <= 122))
                {
                    string _char = ((char)codigo).ToString();
                    if (!senha.Contains(_char))
                    {
                        senha += _char;
                    }
                    else
                    {
                        i--;
                    }
                }
                else
                {
                    i--;
                }
            }
            return senha;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    }
