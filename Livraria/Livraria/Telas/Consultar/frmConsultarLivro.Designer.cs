﻿namespace Livraria.Telas.Consultar
{
    partial class frmConsultarLivro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtLivro = new System.Windows.Forms.TextBox();
            this.dgvConsultarLivro = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConsultarLivro = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarLivro)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(-3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(287, 263);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txtLivro
            // 
            this.txtLivro.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.txtLivro.ForeColor = System.Drawing.Color.DarkViolet;
            this.txtLivro.Location = new System.Drawing.Point(91, 18);
            this.txtLivro.Name = "txtLivro";
            this.txtLivro.Size = new System.Drawing.Size(135, 20);
            this.txtLivro.TabIndex = 7;
            // 
            // dgvConsultarLivro
            // 
            this.dgvConsultarLivro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConsultarLivro.Location = new System.Drawing.Point(13, 73);
            this.dgvConsultarLivro.Name = "dgvConsultarLivro";
            this.dgvConsultarLivro.Size = new System.Drawing.Size(259, 135);
            this.dgvConsultarLivro.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.DarkViolet;
            this.label1.Location = new System.Drawing.Point(34, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Livro:";
            // 
            // btnConsultarLivro
            // 
            this.btnConsultarLivro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarLivro.ForeColor = System.Drawing.Color.DarkViolet;
            this.btnConsultarLivro.Location = new System.Drawing.Point(13, 229);
            this.btnConsultarLivro.Name = "btnConsultarLivro";
            this.btnConsultarLivro.Size = new System.Drawing.Size(260, 23);
            this.btnConsultarLivro.TabIndex = 4;
            this.btnConsultarLivro.Text = "Consultar";
            this.btnConsultarLivro.UseVisualStyleBackColor = true;
            this.btnConsultarLivro.Click += new System.EventHandler(this.btnConsultarLivro_Click_1);
            // 
            // frmConsultarLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txtLivro);
            this.Controls.Add(this.dgvConsultarLivro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnConsultarLivro);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmConsultarLivro";
            this.Text = "frmConsultarLivro";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultarLivro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtLivro;
        private System.Windows.Forms.DataGridView dgvConsultarLivro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConsultarLivro;
    }
}