﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Consultar
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                string usu = txtUsuario.Text;
                string senha = txtSenha.Text;
                string email = txtEmail.Text;

                Business.usuarioBusiness usuarioBusiness = new Business.usuarioBusiness();
                bool contem = usuarioBusiness.Login(usu, senha, email);

                if (contem == true)
                {
                    frmMenu start = new frmMenu();
                    start.Show();
                }
                else
                {
                    throw new ArgumentException("Esse usuário não existe");
                }

            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!");
            }
        }

        private void lblRecuperar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Outros.frmEsqueceuSenha s = new Outros.frmEsqueceuSenha();
            s.Show();
        }

        private void lblCadastrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Inserir.frmCadastrarCliente a = new Inserir.frmCadastrarCliente();
            a.Show();
        }
    }
}
