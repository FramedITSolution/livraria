﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Consultar
{
    public partial class frmConsultarLivro : Form
    {
        public frmConsultarLivro()
        {
            InitializeComponent();
        }

       

        private void btnConsultarLivro_Click(object sender, EventArgs e)
        {
            string NomeLivro = txtLivro.Text;

            Business.livroBusiness db = new Business.livroBusiness();
            List<Database.Entity.tb_livro> livro = db.ConsultarLivro(NomeLivro);

            dgvConsultarLivro.DataSource = livro;
        }

        private void btnConsultarLivro_Click_1(object sender, EventArgs e)
        {
            string NomeLivro = txtLivro.Text;

            Business.livroBusiness db = new Business.livroBusiness();
            List<Database.Entity.tb_livro> livro = db.ConsultarLivro(NomeLivro);

            dgvConsultarLivro.DataSource = livro;
        }
    }
}
