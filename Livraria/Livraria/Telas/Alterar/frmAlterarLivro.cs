﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria.Telas.Alterar
{
    public partial class frmAlterarLivro : Form
    {
        public frmAlterarLivro()
        {
            InitializeComponent();
        }

      
        private void btnAlterarLivro_Click(object sender, EventArgs e)
        {
            Database.Entity.tb_livro db = new Database.Entity.tb_livro();
            db.gn_livro = cboGenero.Text;
            db.nm_livro = txtLivro.Text;
            db.nm_editora = txtEditora.Text;

            Business.livroBusiness livro = new Business.livroBusiness();
            livro.AlterarLivro(db);
        }
    }
}
