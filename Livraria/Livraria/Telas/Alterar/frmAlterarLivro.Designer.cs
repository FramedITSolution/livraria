﻿namespace Livraria.Telas.Alterar
{
    partial class frmAlterarLivro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAlterarLivro = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLivro = new System.Windows.Forms.TextBox();
            this.cboGenero = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEditora = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAlterarLivro
            // 
            this.btnAlterarLivro.Location = new System.Drawing.Point(12, 197);
            this.btnAlterarLivro.Name = "btnAlterarLivro";
            this.btnAlterarLivro.Size = new System.Drawing.Size(228, 23);
            this.btnAlterarLivro.TabIndex = 0;
            this.btnAlterarLivro.Text = "Alterar livro";
            this.btnAlterarLivro.UseVisualStyleBackColor = true;
            this.btnAlterarLivro.Click += new System.EventHandler(this.btnAlterarLivro_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Livro:";
            // 
            // txtLivro
            // 
            this.txtLivro.Location = new System.Drawing.Point(85, 40);
            this.txtLivro.Name = "txtLivro";
            this.txtLivro.Size = new System.Drawing.Size(121, 20);
            this.txtLivro.TabIndex = 2;
            // 
            // cboGenero
            // 
            this.cboGenero.FormattingEnabled = true;
            this.cboGenero.Items.AddRange(new object[] {
            "Ação",
            "Aventura",
            "Terror",
            "Romance",
            "Drama",
            "Motivacional",
            "Educacional"});
            this.cboGenero.Location = new System.Drawing.Point(85, 78);
            this.cboGenero.Name = "cboGenero";
            this.cboGenero.Size = new System.Drawing.Size(121, 21);
            this.cboGenero.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Editora:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Genêro:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtEditora
            // 
            this.txtEditora.Location = new System.Drawing.Point(85, 119);
            this.txtEditora.Name = "txtEditora";
            this.txtEditora.Size = new System.Drawing.Size(121, 20);
            this.txtEditora.TabIndex = 6;
            // 
            // frmAlterarLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 237);
            this.Controls.Add(this.txtEditora);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboGenero);
            this.Controls.Add(this.txtLivro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAlterarLivro);
            this.Name = "frmAlterarLivro";
            this.Text = "frmAlterarLivro";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAlterarLivro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLivro;
        private System.Windows.Forms.ComboBox cboGenero;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEditora;
    }
}