﻿namespace Livraria.Telas.Remover
{
    partial class frmRemoverLivro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudRemoverID = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudRemoverID)).BeginInit();
            this.SuspendLayout();
            // 
            // nudRemoverID
            // 
            this.nudRemoverID.Location = new System.Drawing.Point(78, 89);
            this.nudRemoverID.Name = "nudRemoverID";
            this.nudRemoverID.Size = new System.Drawing.Size(120, 20);
            this.nudRemoverID.TabIndex = 1;
            this.nudRemoverID.ValueChanged += new System.EventHandler(this.nudRemoverID_ValueChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(105, 173);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Remover";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmRemoverLivro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(291, 243);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.nudRemoverID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRemoverLivro";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmRemoverLivro";
            ((System.ComponentModel.ISupportInitialize)(this.nudRemoverID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudRemoverID;
        private System.Windows.Forms.Button button2;
    }
}
