﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria.Database
{
    class livroDatabase
    {

        Entity.livrariaEntities db = new Entity.livrariaEntities();

        public void InserirUsuário(Database.Entity.tb_usuario usuario)
        {
            db.tb_usuario.Add(usuario);
            db.SaveChanges();
        }
        public void InserirLivro(Database.Entity.tb_livro livro)
        {
            db.tb_livro.Add(livro);
            db.SaveChanges();
        }
        public List<Database.Entity.tb_livro> ConsultarLivro(string NomeLivro)
        {
            List<Database.Entity.tb_livro> livro = db.tb_livro.Where(x => x.nm_livro == NomeLivro).ToList();
            return livro;
        }

        public void AlterarLivro(string nome, string genero, string editora, int id)
        {
            Livraria.Database.Entity.tb_livro livro = db.tb_livro.First(x => x.id_livro == id);
            livro.nm_livro = nome;
            livro.gn_livro = genero;
            livro.nm_editora = editora;

            db.SaveChanges();
        }

        public void Remover(int id)
        {
            Entity.tb_livro t = db.tb_livro.First(x => x.id_livro == id);
            db.tb_livro.Remove(t);
            db.SaveChanges();
        }
    }
}
