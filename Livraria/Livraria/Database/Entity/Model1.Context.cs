﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Livraria.Database.Entity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class livrariaEntities : DbContext
    {
        public livrariaEntities()
            : base("name=livrariaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }


        public DbSet<tb_livro> tb_livro { get; set; }
        public DbSet<tb_usu_livro> tb_usu_livro { get; set; }
        public DbSet<tb_usuario> tb_usuario { get; set; }
    }
}
