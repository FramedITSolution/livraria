//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Livraria.Database.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_livro
    {
        public int id_livro { get; set; }
        public string nm_livro { get; set; }
        public string gn_livro { get; set; }
        public string nm_editora { get; set; }
        public Nullable<System.DateTime> lc_livro { get; set; }
    }
}
